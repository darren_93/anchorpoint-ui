import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User, AnchorpointAuthService } from './services/anchorpoint-auth.service';
import { Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'anchorpoint-ui';
  user$: Observable<User>
  userSub: Subscription
  
  constructor(private auth: AnchorpointAuthService, private router: Router, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon(
      'green_box',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/Green.svg')
    )
    this.matIconRegistry.addSvgIcon(
      'blue_box',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/Blue.svg')
    )
    this.matIconRegistry.addSvgIcon(
      'red_box',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/Red.svg')
    )
  }

  ngOnInit(){
    this.user$ = this.auth.getUser()
    this.userSub = this.auth.getUser().subscribe((user)=>{
      if(user == undefined || user == null){
        this.router.navigate(['/'])
      }
    })
  }

  ngOnDestroy(){
    this.userSub.unsubscribe()
  }

  navigateHome(){
    this.router.navigate(['/', 'home'])
  }

  navigateToIFRS9(){
    this.router.navigate(['/', 'ifrs9'])
  }
  
  navigateToRisk(){
    this.router.navigate(['/', 'risk'])
  }

  navigateToManageUsers(){
    this.router.navigate(['/', 'users'])
  }

  navigateToManageOrganisations(){
    this.router.navigate(['/', 'organisations'])
  }

  navigateToUserSettings(){
    console.log('navigate to user settings')
  }

  navigateToAuth(){
    this.router.navigate(['/', 'auth'])
  }

  navigateToIndex(){
    this.router.navigate(['/'])
  }
}
