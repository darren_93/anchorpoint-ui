import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UserManagementPageComponent } from './pages/user-management-page/user-management-page.component';
import { OrganisationManagementPageComponent } from './pages/organisation-management-page/organisation-management-page.component';
import { DisclaimerPageComponent } from './pages/disclaimer-page/disclaimer-page.component';
import { Ifrs9PageComponent } from './pages/ifrs9-page/ifrs9-page.component';
import { RiskCalculationPageComponent } from './pages/risk-calculation-page/risk-calculation-page.component';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';
import { Ifrs9DocumentPageComponent } from './pages/ifrs9-document-page/ifrs9-document-page.component';

const routes: Routes = [
  { path: '', component: IndexPageComponent },
  { path: 'home', component: HomePageComponent },
  { path: 'users', component: UserManagementPageComponent },
  { path: 'organisations', component: OrganisationManagementPageComponent },
  { path: 'disclaimer', component: DisclaimerPageComponent },
  { path: 'ifrs9', children: [
    { path: '', component: Ifrs9PageComponent },
    { path: ':id', component: Ifrs9DocumentPageComponent }
  ] },
  { path: 'risk', component: RiskCalculationPageComponent },
  { path: 'auth', component: AuthPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
