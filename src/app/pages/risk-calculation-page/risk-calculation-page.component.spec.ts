import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskCalculationPageComponent } from './risk-calculation-page.component';

describe('RiskCalculationPageComponent', () => {
  let component: RiskCalculationPageComponent;
  let fixture: ComponentFixture<RiskCalculationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskCalculationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskCalculationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
