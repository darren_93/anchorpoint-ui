import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ifrs9DocumentPageComponent } from './ifrs9-document-page.component';

describe('Ifrs9DocumentPageComponent', () => {
  let component: Ifrs9DocumentPageComponent;
  let fixture: ComponentFixture<Ifrs9DocumentPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ifrs9DocumentPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ifrs9DocumentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
