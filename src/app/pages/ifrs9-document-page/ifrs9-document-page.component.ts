import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Document, AnchorpointIfrs9Service } from 'src/app/services/anchorpoint-ifrs9.service';
import { switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-ifrs9-document-page',
  templateUrl: './ifrs9-document-page.component.html',
  styleUrls: ['./ifrs9-document-page.component.css']
})
export class Ifrs9DocumentPageComponent implements OnInit {

  id$

  constructor(private route: ActivatedRoute, private router: Router, private ifrs9: AnchorpointIfrs9Service) { }

  ngOnInit() {
    this.id$ = this.route.paramMap.pipe(
      map((params: ParamMap)=>{
        return params.get('id')
      })
    )
  }

  up(){
    this.router.navigate(['ifrs9'])
  }

}
