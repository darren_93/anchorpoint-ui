import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationManagementPageComponent } from './organisation-management-page.component';

describe('OrganisationManagementPageComponent', () => {
  let component: OrganisationManagementPageComponent;
  let fixture: ComponentFixture<OrganisationManagementPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationManagementPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationManagementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
