import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ifrs9PageComponent } from './ifrs9-page.component';

describe('Ifrs9PageComponent', () => {
  let component: Ifrs9PageComponent;
  let fixture: ComponentFixture<Ifrs9PageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ifrs9PageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ifrs9PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
