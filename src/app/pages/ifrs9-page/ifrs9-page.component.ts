import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ifrs9-page',
  templateUrl: './ifrs9-page.component.html',
  styleUrls: ['./ifrs9-page.component.css']
})
export class Ifrs9PageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openDocument(id: string){
    this.router.navigate(['ifrs9', id])
  }

}
