import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';
import { DisclaimerPageComponent } from './pages/disclaimer-page/disclaimer-page.component';
import { UserManagementPageComponent } from './pages/user-management-page/user-management-page.component';
import { OrganisationManagementPageComponent } from './pages/organisation-management-page/organisation-management-page.component';
import { Ifrs9PageComponent } from './pages/ifrs9-page/ifrs9-page.component';
import { RiskCalculationPageComponent } from './pages/risk-calculation-page/risk-calculation-page.component';
import { AuthComponent } from './components/auth/auth.component';
import { DisclaimerComponent } from './components/disclaimer/disclaimer.component';
import { HomeComponent } from './components/home/home.component';
import { Ifrs9Component } from './components/ifrs9/ifrs9.component';
import { IndexComponent } from './components/index/index.component';
import { RiskCalculationComponent } from './components/risk-calculation/risk-calculation.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { OrganisationManagementComponent } from './components/organisation-management/organisation-management.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AngularCommonModule } from './angular-common/angular-common.module';
import { OrganisationFormComponent } from './components/organisation-form/organisation-form.component';
import { OrganisationDialogComponent } from './components/organisation-dialog/organisation-dialog.component';
import { Ifrs9SummaryComponent } from './components/ifrs9-summary/ifrs9-summary.component';
import { Ifrs9DocumentPageComponent } from './pages/ifrs9-document-page/ifrs9-document-page.component';
import { Ifrs9DocumentComponent } from './components/ifrs9-document/ifrs9-document.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    HomePageComponent,
    AuthPageComponent,
    DisclaimerPageComponent,
    UserManagementPageComponent,
    OrganisationManagementPageComponent,
    Ifrs9PageComponent,
    RiskCalculationPageComponent,
    AuthComponent,
    DisclaimerComponent,
    HomeComponent,
    Ifrs9Component,
    IndexComponent,
    RiskCalculationComponent,
    UserManagementComponent,
    OrganisationManagementComponent,
    NavbarComponent,
    OrganisationFormComponent,
    OrganisationDialogComponent,
    Ifrs9SummaryComponent,
    Ifrs9DocumentPageComponent,
    Ifrs9DocumentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    FlexLayoutModule,
    AppRoutingModule,
    AngularCommonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    OrganisationDialogComponent
  ]
})
export class AppModule { }
