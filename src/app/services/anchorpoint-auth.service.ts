import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { share } from 'rxjs/operators'
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class AnchorpointAuthService {

  private _userSubject: Subject<User> = new ReplaySubject(undefined)

  set token(value: string){
    if(value !== undefined){
      localStorage.setItem('token', value)
    } else {
      localStorage.removeItem('token')
    }
  }

  get token() {
    return localStorage.getItem('token')
  }

  constructor(private http: HttpClient) { 
    let token = this.token
    if(token){
      this.token = token
      let user: User = jwt_decode(token)
      
      let expires = user.exp * 1000
      let now = new Date().getTime()
      
      let timeuntil = expires - now;
      if(timeuntil > 0){
        setTimeout(()=>{
          this._userSubject.next()
          this.token = undefined
        }, timeuntil)
        this._userSubject.next(user)
      } else {
        localStorage.removeItem('token')
        this._userSubject.next()
      }
    }
  }

  signIn(email: String, password: String): Promise<User>{
    return new Promise((resolve, reject)=>{
      this.http.post('http://localhost:3000/api/Users/signin', {
        email,
        password
      }).subscribe((token: string)=>{
        this.token = token
        let user: User = jwt_decode(token)
        
        let expires = user.exp * 1000
        let now = new Date().getTime()
        
        let timeuntil = expires - now;
        
        setTimeout(()=>{
          this._userSubject.next()
          this.token = undefined
        }, timeuntil)

        this._userSubject.next(user)
        resolve(user)
      }, (err)=>{
        console.log('err', err)
        if(err.status === 0){
          reject({ message: 'Couldn\'t connect to server'})
        } else {
          reject(err)
        }
      })
    })
    
  }

  getUser(): Observable<User> {
    return this._userSubject.asObservable().pipe(share())
  }

  signOut(): Promise<void>{
    return new Promise((resolve, reject)=>{
      if(this.token){
        this.http.get('http://localhost:3000/api/Users/signout', {
          headers: {
            Authorization: `Bearer ${ this.token }`
          }
        }).subscribe(()=>{
          this._userSubject.next()
          resolve()
        }, err=>{
          reject(err)
        })
      } else {
        resolve()
      }
    })
  }
}

export class User {
  email: string
  exp: number
  iat: number
  id: string
  role: Role
  organisation: string
}

export enum Role {
  SYSTEM_ADMINISTRATOR= 'System-Administrator',
  ORGANISATION_ADMINISTRATOR= 'Organisation-Administrator',
  USER= 'User'
}
