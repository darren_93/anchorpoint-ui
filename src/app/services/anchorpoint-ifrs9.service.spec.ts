import { TestBed } from '@angular/core/testing';

import { AnchorpointIfrs9Service } from './anchorpoint-ifrs9.service';

describe('AnchorpointIfrs9Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnchorpointIfrs9Service = TestBed.get(AnchorpointIfrs9Service);
    expect(service).toBeTruthy();
  });
});
