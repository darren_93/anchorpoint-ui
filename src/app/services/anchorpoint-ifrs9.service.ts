import { Injectable } from '@angular/core';
import { AnchorpointAuthService } from './anchorpoint-auth.service';
import { HttpClient } from '@angular/common/http';
import { Subject, throwError, Observable } from 'rxjs';

const API_URL = 'http://localhost:3000'

@Injectable({
  providedIn: 'root'
})
export class AnchorpointIfrs9Service {

  constructor(private auth: AnchorpointAuthService, private http: HttpClient) { }

  getDocuments(): Observable<Document[]> {
    let token = this.auth.token
    let documentsSubject: Subject<Document[]> = new Subject()

    if (token == undefined || token == null) {
      return throwError({ message: 'Not signed in' })
    } else {
      this.http.get(`${API_URL}/api/IFRS9/Document`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((docs: Document[]) => {
        documentsSubject.next(docs)
        documentsSubject.complete()
      }, err => {
        if (err.error.message) {
          documentsSubject.error({ message: err.error.message })
        } else {
          documentsSubject.error({ message: 'Something went wrong' })
        }
      })
      return documentsSubject.asObservable()
    }
  }

  getDocument(id: string): Observable<Document> {
    let token = this.auth.token
    let documentSubject: Subject<Document> = new Subject()

    if (token == undefined || token == null) {
      return throwError({ message: 'Not signed in' })
    } else {
      this.http.get(`${API_URL}/api/IFRS9/Document/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((doc: Document) => {
        documentSubject.next(doc)
        documentSubject.complete()
      }, err => {
        if (err.error.message) {
          documentSubject.error({ message: err.error.message })
        } else {
          documentSubject.error({ message: 'Something went wrong' })
        }
      })
      return documentSubject.asObservable()
    }
  }

  addDocument(): Promise<Document> {
    let token = this.auth.token
    return new Promise((resolve, reject) => {
      if (token == undefined || token == null) {
        reject({ message: 'Not signed in' })
      } else {
        this.http.post(`${API_URL}/api/IFRS9/Document`, {}, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }).subscribe((document: Document) => {
          resolve(document)
        }, (err) => {
          console.log('err', err)
          reject(err)
        })
      }
    })
  }

  deleteDocument(id: string): Promise<void> {
    let token = this.auth.token
    return new Promise((resolve, reject) => {
      if (token == undefined || token == null) {
        reject({ message: 'Not signed in' })
      } else {
        this.http.delete(`${API_URL}/api/IFRS9/Document/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }).subscribe(() => {
          resolve()
        }, (err) => {
          console.log('err', err)
          reject(err)
        })
      }
    })
  }

  uploadClientInfo(documentId: string, file: File): Promise<ClientInfo[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/ClientInfo/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: ClientInfo[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading client info', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadArrearStatus(documentId: string, file: File): Promise<ArrearStatus[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/ArrearStatus/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: ArrearStatus[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading arrear status', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadAssetClass(documentId: string, file: File): Promise<AssetClass[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/AssetClass/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: AssetClass[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading asset class', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadBondCouponRate(documentId: string, file: File): Promise<BondCouponRate[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/BondCouponRate/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: BondCouponRate[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading bond coupon rate', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadCashFlow(documentId: string, file: File): Promise<CashFlow[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/CashFlow/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: CashFlow[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading cash flow', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadCollateral(documentId: string, file: File): Promise<Collateral[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/Collateral/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: Collateral[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading collateral', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadCurrency(documentId: string, file: File): Promise<Currency[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/Currency/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: Currency[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading currency', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadMasterRatingScale(documentId: string, file: File): Promise<MasterRatingScale[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/MasterRatingScale/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: MasterRatingScale[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading master rating scale', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadProbabilityOfDefault(documentId: string, file: File): Promise<ProbabilityOfDefault[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/ProbabilityOfDefault/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: ProbabilityOfDefault[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading probability of default', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadProduct(documentId: string, file: File): Promise<Product[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/Product/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: Product[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading product', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadRiskGrade(documentId: string, file: File): Promise<RiskGrade[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/RiskGrade/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: RiskGrade[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading risk grade', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadScenario(documentId: string, file: File): Promise<Scenario[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/Scenario/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: Scenario[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading scenario', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadTriggerDate(documentId: string, file: File): Promise<TriggerDate[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/TriggerDate/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: TriggerDate[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading trigger date', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }

  uploadUnsecuredLGD(documentId: string, file: File): Promise<UnsecuredLGD[]> {
    let token = this.auth.token
    const formData: FormData = new FormData()
    formData.append('file', file)

    return new Promise((resolve, reject) => {

      this.http.post(`${API_URL}/api/IFRS9/UnsecuredLGD/${documentId}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).subscribe((data: UnsecuredLGD[]) => {
        resolve(data)
      }, (err) => {
        if(err.status == 0 || err.status == 404){
          reject({
            message: 'Could not connect to server'
          })
        } else {
          console.error('error uploading unsecured lgd', err)
          reject({
            message: 'Unknown error'
          })
        }
      })
    })
  }
}

export class Document {
  _id: string
  user: string
  startDate: Date
}

export class ClientInfo {
  _id: string
  PDTermStructureDescription: string
  accountNumber: string
  arrearsStatus: string
  assetClass: string
  calculationApproach: string
  collateralRealisationCurrency: string
  collateralType: string
  collateralValue: number
  contingentProductIndicator: boolean
  countryCeilingRiskGrade: number
  countryOfRisk: string
  customerIdentifier: string
  document: string
  grossCarryingAmount: number
  guarantorUnsecuredLGD: number
  interestRate: number
  lifetimeCCF: number
  line: number
  maturityDate: Date
  minimumSecuredLGD: number
  orginationDate: Date
  originationRiskGrade: number
  paymentAmount: number
  paymentFrequency: number
  paymentTerms: string
  productType: string
  reportCurrency: string
  reportDate: Date
  residualAmount: number
  riskGrade: number
  sectorClassification: string
  settlementCurrency: string
  stageIndicator: number
  totalLimit: number
  twelveMonthCCF: number
  unsecuredLGD: number
  updatedFromDefault: boolean
  user: string
  watchlistIndicator: boolean
}

export class ArrearStatus {
  _id: string
  StageIndicator: number
  arrearsStatusCategory: string
  countryOfRisk: string
  document: string
  extractDate: Date
  line: number
  user: string
}

export class AssetClass {
  assetClassName: string
  countryOfRisk: string
  document: string
  line: number
  minimumSecuredLGD: number
  riskGrade: number
  scenario: string
  unsecuredLGD: string
  user: string
  _id: string
}

export class BondCouponRate {
  line: number
  user: string
  document: string
  organisation: string
  extractDate: Date
  customerIdentifier: string
  accountNumber: string
  couponRate: number
  couponPaymentFrequency: number
  nextCouponDate: Date
  _id: string
}

export class CashFlow {
  line: number
  user: string
  document: string
  organisation: string
  reportDate: Date
  accountNumber: string
  date: Date
  cashFlowAmount: number
  _id: string
}

export class Collateral {
  line: number
  user: string
  document: string
  organisation: string
  valuationDate: Date
  scenario: string
  collateralID: string
  customerIdentifier: string
  accountNumber: string
  collateralType: string
  collateralMarketValue: number
  collateralValue: number
  reportCurrency: string
  collateralRealisationCurrency: string
  _id: string
}

export class Currency {
  line: number
  user: string
  document: string
  organisation: string
  currencyDate: Date
  countryOfRisk: string
  scenario: string
  currency1: string
  currency2: string
  pairRate: number
  _id: string
}

export class MasterRatingScale {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  riskGrade: number
  fittedPD: number
  lowerBound: number
  upperBound: number
}

export class ProbabilityOfDefault {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  termStructureDate: Date
  countryOfRisk: string
  scenario: string
  PDTermStructureDescription: string
  riskGrade: number
  period: number
  PITPD: number
  TTCPD: number
}

export class Product {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  productType: string
  scenario: string
  countryOfRisk: string
  twelveMonthCCF: number
  lifetimeCCF: number
  term: number
  interestRate: number
  riskGrade: number
  unsecuredLGD: number
  minimumSecuredLGD: number
  paymentTerms: string
  paymentFrequency: number
  contingentProductIndicator: boolean
}

export class RiskGrade {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  extractDate: Date
  customerIdentifier: string
  ratingDate: Date
  riskGradeValue: number
}

export class Scenario {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  scenarioDate: Date
  countryofRisk: string
  scenarioName: string
  scenarioWeight: number
}

export class TriggerDate {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  reportDate: Date
  customerIdentifier: string
  accountNumber: string
  triggerDateValue: Date
  triggerDateRiskGrade: number
  currentRiskGrade: number
}

export class UnsecuredLGD {
  _id: string
  line: number
  user: string
  document: string
  organisation: string
  termStructureDate: Date
  countryOfRisk: string
  scenario: string
  LGDTermStructureDescription: string
  period: number
  PITLGD: number
  LRALGD: number
}