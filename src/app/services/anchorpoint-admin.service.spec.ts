import { TestBed } from '@angular/core/testing';

import { AnchorpointAdminService } from './anchorpoint-admin.service';

describe('AnchorpointAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnchorpointAdminService = TestBed.get(AnchorpointAdminService);
    expect(service).toBeTruthy();
  });
});
