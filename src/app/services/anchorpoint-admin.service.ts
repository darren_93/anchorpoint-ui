import { Injectable } from '@angular/core';
import { AnchorpointAuthService, User } from './anchorpoint-auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';

const API_URL = 'http://localhost:3000'

@Injectable({
  providedIn: 'root'
})
export class AnchorpointAdminService {

  constructor(private auth: AnchorpointAuthService, private http: HttpClient) { }

  getOrganisations(): Observable<Organisation[]>{
    let token = this.auth.token
    if(token == null || token == undefined){
      return throwError({ message: 'Not signed in'})
    } else {
      let orgSubject = new Subject<Organisation[]>()
      this.http.get(`${ API_URL }/api/Organisations`, {
        headers: {
          Authorization: `Bearer ${ token }`
        }
      }).subscribe((orgs)=>{
        orgSubject.next(orgs as Organisation[])
        orgSubject.complete()
      }, err=>{
        if(err.error.message){
          orgSubject.error({ message: err.error.message })
        } else {
          orgSubject.error({ message: 'Something went wrong' })
        }
      })
      return orgSubject.asObservable()
    }
  }

  getUsers(): Observable<User[]> {
    let token = this.auth.token
    if(token == null || token == undefined){
      return throwError({ message: 'Not signed in'})
    } else {
      let usersSubject = new Subject<User[]>()
      this.http.get(`${ API_URL }/api/Users`, {
        headers: {
          Authorization: `Bearer ${ token }`
        }
      }).subscribe((users)=>{
        usersSubject.next(users as User[])
        usersSubject.complete()
      }, err=>{
        if(err.error.message){
          usersSubject.error({ message: err.error.message })
        } else {
          usersSubject.error({ message: 'Something went wrong' })
        }
      })
      return usersSubject.asObservable()
    }
  }
}


export class Organisation {
  _id?: string
  name: string
  sheets: Sheet[]
}

export class Sheet {
  name: string
  enabled: boolean
}
