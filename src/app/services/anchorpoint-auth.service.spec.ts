import { TestBed } from '@angular/core/testing';

import { AnchorpointAuthService } from './anchorpoint-auth.service';

describe('AnchorpointAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnchorpointAuthService = TestBed.get(AnchorpointAuthService);
    expect(service).toBeTruthy();
  });
});
