import { Component, OnInit } from '@angular/core';
import { AnchorpointAdminService, Organisation } from 'src/app/services/anchorpoint-admin.service';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { OrganisationDialogComponent } from '../organisation-dialog/organisation-dialog.component';

@Component({
  selector: 'app-organisation-management',
  templateUrl: './organisation-management.component.html',
  styleUrls: ['./organisation-management.component.css']
})
export class OrganisationManagementComponent implements OnInit {

  constructor(private admin: AnchorpointAdminService, public dialog: MatDialog) { }

  organisations$: Observable<Organisation[]>

  columnsToDisplay = ['id', 'name']

  ngOnInit() {
    this.organisations$ = this.admin.getOrganisations()
  }

  addOrganisation(){
    let dialogRef = this.dialog.open(OrganisationDialogComponent)

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result)
    })
  }

  openOrganisation(organisation: Organisation){
    let dialogRef = this.dialog.open(OrganisationDialogComponent, {
      data: organisation
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result)
    })
  }
}
