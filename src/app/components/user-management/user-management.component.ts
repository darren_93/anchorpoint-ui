import { Component, OnInit } from '@angular/core';
import { AnchorpointAdminService } from 'src/app/services/anchorpoint-admin.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/services/anchorpoint-auth.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  users$: Observable<User[]>
  
  columnsToDisplay = ['email', 'role', 'organisation']


  constructor(private admin: AnchorpointAdminService) { }

  ngOnInit() {
    this.users$ = this.admin.getUsers()
  }

}
