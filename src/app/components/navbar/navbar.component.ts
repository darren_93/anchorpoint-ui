import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { User, Role, AnchorpointAuthService } from 'src/app/services/anchorpoint-auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input('user') user: User
  @Output('home') homeEmitter: EventEmitter<void> = new EventEmitter()
  @Output('ifrs9')ifrs9Emitter: EventEmitter<void> = new EventEmitter()
  @Output('risk')riskEmitter: EventEmitter<void> = new EventEmitter()
  @Output('manage-organisations')manageOrganisationsEmitter: EventEmitter<void> = new EventEmitter()
  @Output('manage-users')manageUsersEmitter: EventEmitter<void> = new EventEmitter()
  @Output('user-settings')userSettingsEmitter: EventEmitter<void> = new EventEmitter()
  @Output('signin')signinEmitter: EventEmitter<void> = new EventEmitter()
  @Output('signout')signoutEmitter: EventEmitter<void> = new EventEmitter()

  constructor(private auth: AnchorpointAuthService) { }

  ngOnInit() {
  }

  homeClicked(){
    this.homeEmitter.emit()
  }

  ifrs9Clicked(){
    this.ifrs9Emitter.emit()
  }

  riskClicked(){
    this.riskEmitter.emit()
  }

  userSettingsClicked(){
    this.userSettingsEmitter.emit()
  }

  manageUsersClicked(){
    this.manageUsersEmitter.emit()
  }

  manageOrganisationsClicked(){
    this.manageOrganisationsEmitter.emit()
  }

  signInClicked(){
    this.signinEmitter.emit()
  }

  signOutClicked(){
    this.auth.signOut().then(()=>{
      this.signoutEmitter.emit()
    }, (err)=>{
      console.log('err', err)
    })
  }

  isSysAdmin(user: User){
    return user.role === Role.SYSTEM_ADMINISTRATOR
  }

  isOrgAdmin(user: User){
    return user.role === Role.ORGANISATION_ADMINISTRATOR
  }

}
