import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AnchorpointAuthService } from 'src/app/services/anchorpoint-auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  processing = false
  @Output('forgotPassword') forgotPasswordEmitter: EventEmitter<void> = new EventEmitter()
  @Output('signedIn') signedInEmitter: EventEmitter<void> = new EventEmitter()

  email: string
  password: string

  constructor(private auth: AnchorpointAuthService, private snackbar: MatSnackBar) { }

  ngOnInit() {
  }

  signIn(email, password){
    this.processing = true
    this.auth.signIn(email, password).then((user)=>{
      this.processing = false
      this.signedInEmitter.emit()
    }, (err)=>{
      this.processing = false
      if(err.error && err.error.message){
        this.snackbar.open(err.error.message, 'OK')
      } else if (err.message) {
        this.snackbar.open(err.message, 'OK')
      } else {
        this.snackbar.open('Something went wrong', 'OK')
      }
    })
  }

  forgotPassword(){
    this.forgotPasswordEmitter.emit()
  }

}
