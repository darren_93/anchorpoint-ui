import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Organisation } from 'src/app/services/anchorpoint-admin.service';

@Component({
  selector: 'app-organisation-form',
  templateUrl: './organisation-form.component.html',
  styleUrls: ['./organisation-form.component.css']
})
export class OrganisationFormComponent implements OnInit {

  constructor() { }

  ownerEmail
  @Input() organisation: Organisation
  @Output() organisationChange: EventEmitter<Organisation> = new EventEmitter()
  @Output('update') update: EventEmitter<void> = new EventEmitter()
  @Output('cancel') cancel: EventEmitter<void> = new EventEmitter()
  @Output('create') create: EventEmitter<string> = new EventEmitter()
  @Output('delete') delete: EventEmitter<void> = new EventEmitter()

  ngOnInit() {
    if (!this.organisation) {
      this.organisation = {
        name: null,
        sheets: [
          { name: "ClientInfo", enabled: true },
          { name: "ProbabilityOfDefault", enabled: true }, 
          { name: "CashFlow", enabled: true }, 
          { name: "MasterRatingScale", enabled: true },
          { name: "Collateral", enabled: true },
          { name: "Product", enabled: true },
          { name: "AssetClass", enabled: true },
          { name: "Scenario", enabled: true },
          { name: "Currency", enabled: true },
          { name: "TriggerDate", enabled: true },
          { name: "RiskGrade", enabled: true },
          { name: "UnsecuredLGD", enabled: true },
          { name: "ArrearsStatus", enabled: true },
          { name: "BondCouponRate", enabled: true }
        ]
      }
    }
  }

  deleteClicked() {
    this.delete.emit()
  }

  cancelClicked() {
    this.cancel.emit()
  }

  createClicked(ownerEmail) {
    this.organisationChange.emit(this.organisation)
    this.create.emit(ownerEmail)
  }

  updateClicked() {
    this.organisationChange.emit(this.organisation)
    this.update.emit()
  }
}
