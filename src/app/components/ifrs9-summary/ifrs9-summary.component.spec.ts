import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ifrs9SummaryComponent } from './ifrs9-summary.component';

describe('Ifrs9SummaryComponent', () => {
  let component: Ifrs9SummaryComponent;
  let fixture: ComponentFixture<Ifrs9SummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ifrs9SummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ifrs9SummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
