import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Document } from 'src/app/services/anchorpoint-ifrs9.service';

@Component({
  selector: 'app-ifrs9-summary',
  templateUrl: './ifrs9-summary.component.html',
  styleUrls: ['./ifrs9-summary.component.css']
})
export class Ifrs9SummaryComponent implements OnInit {

  @Input() document: Document

  constructor() { }

  ngOnInit() {
  }

}
