import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ifrs9Component } from './ifrs9.component';

describe('Ifrs9Component', () => {
  let component: Ifrs9Component;
  let fixture: ComponentFixture<Ifrs9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ifrs9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ifrs9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
