import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AnchorpointIfrs9Service, Document } from 'src/app/services/anchorpoint-ifrs9.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ifrs9',
  templateUrl: './ifrs9.component.html',
  styleUrls: ['./ifrs9.component.css']
})
export class Ifrs9Component implements OnInit {

  documents$: Observable<Document[]>

  @Output('open') openEmitter: EventEmitter<Document> = new EventEmitter() 

  constructor(private ifrs9: AnchorpointIfrs9Service) { }

  ngOnInit() {
    this.documents$ = this.ifrs9.getDocuments()
    
  }

  createNewDocument(){
    this.ifrs9.addDocument().then((document)=>{
      this.documents$ = this.ifrs9.getDocuments()
    }, (err)=>{
      console.log('err', err)
    })
  }
  
  openDocument(document){
    this.openEmitter.emit(document)
  }
}
