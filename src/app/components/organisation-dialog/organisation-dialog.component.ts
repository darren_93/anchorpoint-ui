import { Component, OnInit, Inject } from '@angular/core';
import { Organisation } from 'src/app/services/anchorpoint-admin.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-organisation-dialog',
  templateUrl: './organisation-dialog.component.html',
  styleUrls: ['./organisation-dialog.component.css']
})
export class OrganisationDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<OrganisationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Organisation) { }

  ngOnInit() {
  }

  create(organisation: Organisation, email: string){
    this.dialogRef.close({ data: { organisation, email }, status: OrganisationDialogComponentResult.CREATE })
  }

  delete(organisation: Organisation){
    this.dialogRef.close({ data: organisation, status: OrganisationDialogComponentResult.DELETE })
  }

  update(organisation: Organisation){
    this.dialogRef.close({ data: organisation, status: OrganisationDialogComponentResult.UPDATE })
  }

  cancel(){
    this.dialogRef.close()
  }

}

export enum OrganisationDialogComponentResult {
  CREATE,
  UPDATE,
  DELETE
}
