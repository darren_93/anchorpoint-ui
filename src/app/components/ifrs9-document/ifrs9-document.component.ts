import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { AnchorpointIfrs9Service, Document, ClientInfo, ArrearStatus, AssetClass, CashFlow, BondCouponRate, Collateral, Currency, MasterRatingScale, ProbabilityOfDefault, Product, RiskGrade, Scenario, TriggerDate, UnsecuredLGD } from 'src/app/services/anchorpoint-ifrs9.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-ifrs9-document',
  templateUrl: './ifrs9-document.component.html',
  styleUrls: ['./ifrs9-document.component.css']
})
export class Ifrs9DocumentComponent implements OnInit, OnDestroy {

  @Input() id: string
  @Output('deleted') deletedEmitter: EventEmitter<void> = new EventEmitter()

  document: Document
  documentSub: Subscription

  deleting: boolean = false

  processingClientInfo: boolean = false
  processingArrearStatus: boolean = false
  processingAssetClass: boolean = false
  processingBondCouponRate: boolean = false
  processingCashFlow: boolean = false
  processingCollateral: boolean = false
  processingCurrency: boolean = false
  processingMasterRatingScale: boolean = false
  processingProbabilityOfDefault: boolean = false
  processingProduct: boolean = false
  processingRiskGrade: boolean = false
  processingScenario: boolean = false
  processingTriggerDate: boolean = false
  processingUnsecuredLGD: boolean = false

  constructor(private ifrs9: AnchorpointIfrs9Service) {
  }

  ngOnInit() {
    this.documentSub = this.ifrs9.getDocument(this.id).subscribe((document: Document)=>{
      this.document = document
    })
  }

  ngOnDestroy() {
    if(this.documentSub){
      this.documentSub.unsubscribe()
    }
  }

  openDelete(){
    this.deleting = true
    setTimeout(()=>{
      this.deleting = false
    }, 10000)
  }

  deleteDocument(doc: Document){
    this.ifrs9.deleteDocument(doc._id).then(()=>{
      this.deletedEmitter.emit()
    }).catch((err)=>{
      console.error('error deleting document', err)
    })
  }

  uploadClientInfo(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingClientInfo = true
      this.ifrs9.uploadClientInfo(id, masterFile).then((data: ClientInfo[])=>{
        console.log('client info', data)
      }).catch((err)=>{
        console.log('error upload client info', err)
      }).finally(()=>{
        this.processingClientInfo = false
      })
    }
  }

  uploadArrearStatus(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingArrearStatus = true
      this.ifrs9.uploadArrearStatus(id, masterFile).then((data: ArrearStatus[])=>{
        console.log('arrears statuses', data)
      }).catch((err)=>{
        console.log('error upload arrears status', err)
      }).finally(()=>{
        this.processingArrearStatus = false
      })
    }
  }

  uploadAssetClass(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingAssetClass = true
      this.ifrs9.uploadAssetClass(id, masterFile).then((data: AssetClass[])=>{
        console.log('asset classes', data)
      }).catch((err)=>{
        console.log('error upload asset class', err)
      }).finally(()=>{
        this.processingAssetClass = false
      })
    }
  }

  uploadBondCouponRate(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingBondCouponRate = true
      this.ifrs9.uploadBondCouponRate(id, masterFile).then((data: BondCouponRate[])=>{
        console.log('bond coupon rates', data)
      }).catch((err)=>{
        console.log('error upload bond coupon rate', err)
      }).finally(()=>{
        this.processingBondCouponRate= false
      })
    }
  }

  uploadCashFlow(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingCashFlow = true
      this.ifrs9.uploadCashFlow(id, masterFile).then((data: CashFlow[])=>{
        console.log('cash flow', data)
      }).catch((err)=>{
        console.log('error upload cash flow', err)
      }).finally(()=>{
        this.processingCashFlow = false
      })
    }
  }

  uploadCollateral(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingCollateral = true
      this.ifrs9.uploadCollateral(id, masterFile).then((data: Collateral[])=>{
        console.log('collateral', data)
      }).catch((err)=>{
        console.log('error upload collateral', err)
      }).finally(()=>{
        this.processingCollateral = false
      })
    }
  }

  uploadCurrency(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingCurrency = true
      this.ifrs9.uploadCurrency(id, masterFile).then((data: Currency[])=>{
        console.log('currency', data)
      }).catch((err)=>{
        console.log('error upload currency', err)
      }).finally(()=>{
        this.processingCurrency = false
      })
    }
  }

  uploadMasterRatingScale(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingMasterRatingScale = true
      this.ifrs9.uploadMasterRatingScale(id, masterFile).then((data: MasterRatingScale[])=>{
        console.log('master rating scale', data)
      }).catch((err)=>{
        console.log('error upload master rating scale', err)
      }).finally(()=>{
        this.processingMasterRatingScale = false
      })
    }
  }

  uploadProbabilityOfDefault(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingProbabilityOfDefault = true
      this.ifrs9.uploadProbabilityOfDefault(id, masterFile).then((data: ProbabilityOfDefault[])=>{
        console.log('probabilty of default', data)
      }).catch((err)=>{
        console.log('error upload probabilty of default', err)
      }).finally(()=>{
        this.processingProbabilityOfDefault = false
      })
    }
  }

  uploadProduct(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingProduct = true
      this.ifrs9.uploadProduct(id, masterFile).then((data: Product[])=>{
        console.log('product', data)
      }).catch((err)=>{
        console.log('error upload product', err)
      }).finally(()=>{
        this.processingProduct = false
      })
    }
  }

  uploadRiskGrade(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingRiskGrade = true
      this.ifrs9.uploadRiskGrade(id, masterFile).then((data: RiskGrade[])=>{
        console.log('risk grade', data)
      }).catch((err)=>{
        console.log('error upload risk grade', err)
      }).finally(()=>{
        this.processingRiskGrade = false
      })
    }
  }

  uploadScenario(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingScenario = true
      this.ifrs9.uploadScenario(id, masterFile).then((data: Scenario[])=>{
        console.log('scenario', data)
      }).catch((err)=>{
        console.log('error upload scenario', err)
      }).finally(()=>{
        this.processingScenario = false
      })
    }
  }

  uploadTriggerDate(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingTriggerDate = true
      this.ifrs9.uploadTriggerDate(id, masterFile).then((data: TriggerDate[])=>{
        console.log('trigger date', data)
      }).catch((err)=>{
        console.log('error upload trigger date', err)
      }).finally(()=>{
        this.processingTriggerDate = false
      })
    }
  }

  uploadUnsecuredLGD(masterFile: File){
    let id = this.id
    if(masterFile){
      this.processingUnsecuredLGD = true
      this.ifrs9.uploadUnsecuredLGD(id, masterFile).then((data: UnsecuredLGD[])=>{
        console.log('unsecuredLGD', data)
      }).catch((err)=>{
        console.log('error upload unsecuredLGD', err)
      }).finally(()=>{
        this.processingUnsecuredLGD = false
      })
    }
  }

  uploadMasterFile(masterFile: File){
    this.uploadClientInfo(masterFile)
    this.uploadArrearStatus(masterFile)
    this.uploadAssetClass(masterFile)
    this.uploadBondCouponRate(masterFile)
    this.uploadCashFlow(masterFile)
    this.uploadCollateral(masterFile)
    this.uploadCurrency(masterFile)
    this.uploadMasterRatingScale(masterFile)
    this.uploadProbabilityOfDefault(masterFile),
    this.uploadProduct(masterFile),
    this.uploadRiskGrade(masterFile),
    this.uploadScenario(masterFile),
    this.uploadTriggerDate(masterFile),
    this.uploadUnsecuredLGD(masterFile)
  }

}
