import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ifrs9DocumentComponent } from './ifrs9-document.component';

describe('Ifrs9DocumentComponent', () => {
  let component: Ifrs9DocumentComponent;
  let fixture: ComponentFixture<Ifrs9DocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ifrs9DocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ifrs9DocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
